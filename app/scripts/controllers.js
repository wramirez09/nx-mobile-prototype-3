'use strict';
angular.module('MobileAlpha12.controllers', [])

.controller('DashCtrl', function() {
})

.controller('FriendsCtrl', function($scope, Friends) {
  $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})

.controller('MyhealthCtrl', function() {
	// $scope;
})

.controller('CostCtrl', function() {
	// $scope;
})

.controller('MedicationsCtrl', function() {
	// $scope;
})

.controller('HelpCtrl', function() {
	// $scope;
})

.controller('LoginCtrl', function($scope, $stateParams, $state){
	$scope.login = {};

	$scope.loggedIn = function(){
		$state.go('tab.dash');
		console.log('logged in ');
	};
});

(function () {
    var m = !0, q = !1;
    (function (c) {
        function h(a, b, d, c) { this.j = a; this.f = b; this.q = d; this.s = c; this.n = b + c; this.l = a + d; return this } function s(a, b) { for (; b; ) { if (b == a) return m; b = b.parentElement } return q } function z(a, b) {
            if (b) {
                callbackUrl = b.replace(/%e/, (a.b ? 30 : a.p ? 50 < a.c ? 32 : 31 : 34 + a.r).toString(16).toUpperCase()); callbackUrl = callbackUrl.replace(/%f/, a.a.toString(16).toUpperCase()); var d = "v:1.1.0,iv:" + (a.b ? "NA" : a.c) + ",rand:" + a.g; 0 < a.k && (d += ",tm:" + a.k); callbackUrl = callbackUrl.replace(/%g/, d); "https:" == document.location.protocol && (callbackUrl =
callbackUrl.replace(/^http:/i, "https:")); t(callbackUrl); a.v = m
            } 
        } function F(a, b) { var d; b && (d = b.replace(/%e/, (41).toString(16).toUpperCase()), d = d.replace(/%f/, 0), d = d.replace(/%g/, "v:1.1.0,ao:" + a), "https:" == document.location.protocol && (d = d.replace(/^http:/i, "https:")), t(d)) } function t(a) { var b = new Image(1, 1); b.src = a; b.onload = b.onerror = function () { b.onload = b.onerror = null } } function B(a, b, d, n) {
            b = b || {}; var p = !a, f = 999, r = b.a || 0; if (b.i) {
                var k = new h(screen.availTop || 0, screen.availLeft || 0, screen.availHeight, screen.availWidth),
f = new h(void 0 !== c.screenY ? c.screenY : c.screenTop, void 0 !== c.screenX ? c.screenX : c.screenLeft, void 0 !== c.outerHeight ? c.outerHeight : screen.availHeight, void 0 !== c.outerWidth ? c.outerWidth : screen.availWidth), l; if (p) l = new h("undefined" == typeof c.mozInnerScreenY ? c.screenTop : c.mozInnerScreenY, "undefined" == typeof c.mozInnerScreenX ? c.screenLeft : c.mozInnerScreenX, c.innerHeight, c.innerWidth); else { var e = a.getBoundingClientRect(); l = new h(f.j + e.top, f.f + e.left, n ? n : e.bottom - e.top, d ? d : e.right - e.left) } k = k.e(f); k = l.e(k).d();
                l = l.d(); if (!p && document && document.documentElement) { for (var f = e = 0, g = a; g; ) e += g.offsetLeft, f += g.offsetTop, g = g.offsetParent; d = new h(f, e, n ? n : a.offsetHeight, d ? d : a.offsetWidth); n = new h(c.scrollY || c.pageYOffset || document.body && document.body.scrollTop || 0, c.scrollX || c.pageXOffset || document.body && document.body.scrollLeft || 0, document.documentElement.clientHeight, document.documentElement.clientWidth); d = d.e(n); k = Math.min(k, d.d()) } f = 0 < l ? parseInt(100 * (k / l), 10) : 0; r |= 2
            } if (b.h && (p || 0 < a.offsetWidth && 0 < a.offsetHeight)) p ?
(b = document.elementFromPoint(0, 0), p = document.elementFromPoint(0, c.innerHeight - 1), d = document.elementFromPoint(c.innerWidth - 1, 0), n = document.elementFromPoint(c.innerWidth - 1, c.innerHeight - 1), e = document.elementFromPoint(c.innerWidth / 2, c.innerHeight / 2)) : (e = a.getBoundingClientRect(), b = s(a, document.elementFromPoint(e.left + 1.25, e.top + 1.25)), p = s(a, document.elementFromPoint(e.right - 1.25, e.top + 1.25)), d = s(a, document.elementFromPoint(e.left + 1.25, e.bottom - 1.25)), n = s(a, document.elementFromPoint(e.right - 1.25, e.bottom -
1.25)), e = s(a, document.elementFromPoint((e.left + e.right) / 2, (e.top + e.bottom) / 2))), f = Math.min([0, 25, 50, 90, 99, 100][(b ? 1 : 0) + (p ? 1 : 0) + (d ? 1 : 0) + (n ? 1 : 0) + (e ? 1 : 0)], f), e || (f = Math.min(50, f)), r |= 4; 999 == f && (f = 0); return { o: a, c: f, u: l, w: k, a: r}
        } function C() { var a = c != top, b = { i: !a, h: !a }, d = navigator.userAgent; /msie/i.test(d) || /windows.*trident/i.test(d) ? b.h = m : /firefox/i.test(d) && (b.i = m); b.b = a && !b.i && !b.h; b.a = 0; a && (b.a |= 1); return b } function G() {
            var a, b; try {
                a = "{"; for (var d = 0; d < location.ancestorOrigins.length; d++) a = a + "'" + location.ancestorOrigins.item(d) +
"'", 1 < location.ancestorOrigins.length - d && (a += ","); b = D(a + "}"); re = /=+$/; return b = b.replace(re, "$'")
            } catch (c) { } 
        } function D(a) { return btoa(encodeURIComponent(a).replace(/%([0-9A-F]{2})/g, function (a, d) { return String.fromCharCode("0x" + d) })) } function x(a, b, d, n) {
            function p() { -1 != e && (clearInterval(e), e = -1); -1 != g && (clearTimeout(g), g = -1) } var f = parseInt(99999 * Math.random()), r = 0, k = 0, l = 0, e = -1, g = -1, h = q, s = null, u = 0; if (location.ancestorOrigins) { var t = G(); try { F(t, b) } catch (x) { } } var y = C(); if (y.b) {
                try {
                    z({ o: a, a: y.a, b: m,
                        k: 0, g: f
                    }, b)
                } catch (A) { } return q
            } try { var v = B(a, y, d, n); v.p = m; v.g = f; var E = 50 < v.c; u++; z(v, b); E && (s = new Date); h = E } catch (D) { return q } e = setInterval(function () { try { if (u >= H) return p(), q; var e = new Date, g = B(a, y, d, n), t = 50 < g.c, w = q, v = q; t && (h && (k = e - s), k + r >= c._mpiv.m[l] && (v = m, g.r = l, l++)); g.k = k + r; g.g = f; v && (u++, z(g, b)); t && !h && (s = e); !t && h && (r += k, k = 0); h = t; w = c._mpiv.STOP; !w && !c._mpiv.m[l] && (w = m); w && p(); return !w } catch (x) { return p(), q } }, I); g = setTimeout(p, J)
        } function A(a) {
            Array.prototype.indexOf || (Array.prototype.indexOf =
function (a, b) { for (var c = b || 0, f = this.length; c < f; c++) if (this[c] === a) return c; return -1 }); var b = a ? a : "frame"; if (-1 != u.indexOf(b)) return m; u.push(b); if (a = a && a.id) { if (-1 != u.indexOf(a)) return m; u.push(a) } return q
        } function K(a, b, d, c) { return A(a) || "string" == typeof a && (a = document.getElementById(a), null == a) ? q : x(a, b, d, c) } function L(a) { return A(null) ? q : x(null, a) } h.prototype.e = function (a) {
            var b = Math.max(this.j, a.j), d = Math.max(this.f, a.f), c = Math.min(this.l, a.l); a = Math.min(this.n, a.n); a = Math.max(a - d, 0); c = Math.max(c -
b, 0); return new h(b, d, c, a)
        }; h.prototype.d = function () { return this.s * this.q }; var I = 500, J = 72E5, H = 10, u = []; c._mpiv = c._mpiv || { mf: L, me: K, m: [1E3, 5E3, 1E4, 3E4, 6E4], t: C}
    })(window);
})();
